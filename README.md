# Gestion de mises à jour et lecture automatique de publicité sur les points de vente MF #

## Informations ##
Les publicités sont stockées sur le server SQLSRV64-32 dans le répertoire partagé MF_PUBS.

## Pré-requis ##
 - poste sous Windows (avec les ressources suffisantes pour lire des vidéos 4K toute une journée en continu)
 - VLC installé
 - connexion réseau
 - application updaterW.exe (permet de gérer les mises à jour du script)

## Installation ##
 - importation du fichier xml de tâche planifiée (MF_pubs_tache_planifiée.xml),
 - ajustement en fonction des emplacements des programmes updaterW.exe et VLC.exe
 - exécution de la tâche planifiée

## Exemple d'appel en ligne de commande ##
updaterw.exe %USERPROFILE%\Documents\mf_pubs\mf_pubs.vbs "c:\Program Files\VideoLAN\VLC\vlc.exe" c:\mf_pubs -f